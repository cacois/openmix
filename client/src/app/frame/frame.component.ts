import { Component, OnInit } from '@angular/core';
import { NavigationItemConfig } from 'patternfly-ng';

@Component({
  selector: 'app-frame',
  templateUrl: './frame.component.html',
  styleUrls: ['./frame.component.css']
})
export class FrameComponent implements OnInit {

  navigationItems: NavigationItemConfig[];

  constructor() { }

  ngOnInit() {
    this.navigationItems = [
      {
        title: 'Home',
        iconStyleClass: 'fa fa-dashboard',
        url: '/navigation/dashboard'
      },
      {
        title: 'Actions',
        iconStyleClass: 'fa fa-gears',
        url: '/hello',
        badges: [
          {
            count: 1234,
            tooltip: 'Total number of actions'
          }
        ]
      },
      {
        title: 'Triggers',
        iconStyleClass: 'fa fa-gears',
        url: '/hello',
        badges: [
          {
            count: 1,
            tooltip: 'Total number of triggers'
          }
        ]
      }
    ];
  }

  onItemClicked($event: NavigationItemConfig): void {
    console.log(`clicked: ${JSON.stringify($event)}`);
  }

  onNavigation($event: NavigationItemConfig): void {
    console.log(`navigated: ${JSON.stringify($event)}`);
  }
}
