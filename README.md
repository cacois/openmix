# OpenMix

An open source user interface and management console for serverless, open source cloud platform [Apache OpenWhisk](https://openwhisk.apache.org/).

A web application powered by KoaJS 2 (server) and AngularJS 6 (client). Dockerized and designed with a microservice architecture, served by Nginx. 

## Development Quick Start

### Install OpenWhisk Development Tools

    $ git clone git@github.com:apache/incubator-openwhisk-devtools.git
    $ cd incubator-openwhisk-devtools/docker-compose
    $ make quick-start

This will take 1-2 minutes, at the end of which you will have a large number of docker containers (around 10) running a local development instance of OpenWhisk. Want to verify that its working? Run the following command:

    $ openwhisk-master/bin/wsk -i property get

This will dump the configuration values of the running OpenWhisk instance.

### Run OpenMix

Clone the repository:

    $ git clone git@gitlab.com:cacois/openmix.git

Install dependencies for client and server:

    $ (cd client; yarn)
    $ (cd server; yarn)

Start the server:

    $ cd server
    $ yarn start

The server will start on port 3000. In a new terminal, start the client:

    $ cd client
    $ yarn start

or for hot module reload:

    $ cd client
    $ yarn start:hmr

The client will start on port 4200, and will internally proxy all requests to /api to the server on port 3000 to avoid CORS errors. Note: this will not be a problem in production, since we will be serving both client and server through Nginx.

Develop!

